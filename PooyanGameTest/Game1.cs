﻿namespace PooyanGameTest
{
    using System;
    using Environment;
    using GameObjects;
    using GameObjects.Entities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Utils;

    public class Game1 : Game
    {
        private readonly Random rng = new Random(DateTime.Now.Hour);

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private Level level;
        private Player player;
        private EnemiesManager enemyManager;
        private UI.UI UI;

        public Game1()
        {
            this.Window.Title = "Pooyan";
            this.graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferHeight = 480,
                PreferredBackBufferWidth = 650
            };
            this.Content.RootDirectory = "Content";
        }
        
        protected override void Initialize()
        {
            this.level = new Level(this.Content, Constants.LEVEL_TEXTURE_NAME, this.graphics.PreferredBackBufferWidth, this.graphics.PreferredBackBufferHeight);
            this.player = new Player(this.Content, Constants.PLAYER_TEXTURE_NAME);
            this.enemyManager = new EnemiesManager(this.Content, ref this.player);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            this.UI = new UI.UI(this.Content.Load<SpriteFont>("Graphics/Font"));
        }
        
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            this.UI.Update(this.enemyManager.EnemiesKilled);
            this.player.Update(gameTime, Keyboard.GetState());
            this.enemyManager.Update(gameTime);
            base.Update(gameTime);
        }
       
        protected override void Draw(GameTime gameTime)
        {
            this.spriteBatch.Begin();
            this.level.Draw(this.spriteBatch);
            this.player.Draw(this.spriteBatch);
            this.enemyManager.Draw(this.spriteBatch);
            this.UI.Draw(this.spriteBatch);

            base.Draw(gameTime);
            this.spriteBatch.End();
        }
    }
}
