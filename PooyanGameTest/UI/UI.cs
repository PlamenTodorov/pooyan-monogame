﻿namespace PooyanGameTest.UI
{
    using GameObjects;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using System;
    using Utils;

    public class UI
    {
        public UI(SpriteFont font)
        {
            this.Font = font;
            this.Score = 0;
            this.BusyLadders = Constants.BusyLadders;
        }

        public SpriteFont Font { get; private set; }

        public int Score { get; private set; }

        public int BusyLadders { get; private set; }

        public void Update(int score)
        {
            this.BusyLadders = Constants.BusyLadders;
            this.Score = score;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(this.Font, "Score: " + this.Score, new Vector2(0, 20), Color.AntiqueWhite);
        }
    }
}
