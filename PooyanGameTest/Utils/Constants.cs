﻿namespace PooyanGameTest.Utils
{
    using System;

    public static class Constants
    {
        // Player Constants
        public const string PLAYER_TEXTURE_NAME = "Graphics/Player";
        public const int PLAYER_MAX_TOP_POS_Y = 150;
        public const int PLAYER_MAX_BOT_POS_Y = 330;
        public const int PLAYER_START_POS_Y = 150;
        public const int PLAYER_POS_X = 546;
        public const int PLAYER_START_LIFES = 1;
        public const int PLAYER_SPEED = 5;

        // Rope Constants
        public const string ROPE_TEXTURE_NAME = "Graphics/rope";
        public const int ROPE_POSITION_START_X = 570;
        public const int ROPE_POSITION_START_Y = 111;

        // Arrow Constants
        public const string ARROW_TEXTURE_NAME = "Graphics/Arrow";
        public const int ARROW_RANGE_X = 35;
        public const int ARROW_SPEED = 5;
        public const int ARROW_DELAY_DELETE = 3;

        // Level Constatns
        public const string LEVEL_TEXTURE_NAME = "Graphics/LevelFont";
        public const int GROUND_POSITION_Y = 420;
        public const int LADDER_POSITION_X = 590;
        public static readonly int[] LadderPositionsY = { 350, 285, 220, 160 };

        // Enemies Constants
        public const string ENEMY_WALKING_TEXTURE = "Graphics/EnemyWalkingSheet";
        public const string ENEMY_FALLING_TEXTURE = "Graphics/EnemyDropping";
        public const string ENEMY_DEAD_TEXTURE = "Graphics/EnemyFalling";
        public const string ENEMY_ON_GROUND_WALKING_TEXTURE = "Graphics/EnemyWinWalkingSheet";
        public const string ENEMY_ON_LADDER = "Graphics/EnemyWinClimbingSheet";
        public const int ENEMY_FALL_POSITION_X_LEFT = 100;
        public const int ENEMY_FALL_POSITION_X_RIGHT = 300;
        public const int ENEMY_MIN_SPEED = 2;
        public const int ENEMY_MAX_SPEED = 4;
        public const int ENEMY_MIN_POS_Y = 20;
        public const int ENEMY_MAX_POS_Y = 35;
        public const int ENEMY_MIN_POS_X = 300;

        public static readonly Random Random = new Random(DateTime.Now.Hour);

        public static int BusyLadders { get; set; } = 0;
    }
}
