﻿namespace PooyanGameTest.GameObjects.FlyingObjects
{
    using System;
    using System.Diagnostics;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Utils;

    public class Arrow
    {
        public Arrow(ContentManager content, int speed, Vector2 playerPosition)
        {
            this.Texture = content.Load<Texture2D>(Constants.ARROW_TEXTURE_NAME);
            this.Speed = speed;
            this.Position = playerPosition;
            this.IsActive = true;
            this.HasDeflected = false;
            this.Rotation = 0;
        }

        public bool IsActive { get; private set; }

        public Vector2 Position { get; private set; }

        private Texture2D Texture { get; set; }

        private int Speed { get; set; }

        private DelayedAction DelayedAction { get; set; }

        private bool HasDeflected { get; set; }

        private float Rotation { get; set; }

        public void Update(GameTime gameTime)
        {
            if (this.Position.X >= Constants.ARROW_RANGE_X)
            {
                if (this.HasDeflected)
                {
                    this.Move(new Vector2(0, this.Speed));
                }
                else
                {
                    this.Move(new Vector2(-this.Speed, 0));
                }
            }
            else if (DelayedAction == null)
            {
                this.DelayedAction = new DelayedAction(() => { this.IsActive = false; }, Constants.ARROW_DELAY_DELETE);
            }

            if (DelayedAction != null)
            {
                this.DelayedAction.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
            }
        }

        public void Move(Vector2 relativeDirection)
        {
            this.Position += relativeDirection;
        }

        public void Hit()
        {
            this.IsActive = false;
        }

        public Rectangle CalculateArrowBounds()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.Texture.Width, this.Texture.Height);
        }

        public void Deflect()
        {
            this.Rotation = -8;
            this.HasDeflected = true;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.Texture, this.Position, scale: new Vector2(1.7f, 1.7f), rotation: this.Rotation);
        }
    }
}
