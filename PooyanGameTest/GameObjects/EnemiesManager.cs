﻿namespace PooyanGameTest.GameObjects
{
    using System.Collections.Generic;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using PooyanGameTest.GameObjects.Entities;
    using Utils;

    public class EnemiesManager
    {
        public EnemiesManager(ContentManager content, ref Player playerRef)
        {
            this.Content = content;
            this.PlayerRef = playerRef;
            this.Enemies = new List<Enemy>();
            this.SpawnTimer = 0;
            this.EnemiesKilled = 0;
        }

        public int EnemiesKilled { get; private set; }

        private ContentManager Content { get; set; }

        private Player PlayerRef { get; set; }

        private List<Enemy> Enemies { get; set; }

        private float SpawnTimer { get; set; }

        public void Update(GameTime gameTime)
        {
            this.SpawnTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (this.SpawnTimer >= 1)
            {
                this.SpawnTimer = 0;
                this.GenerateEnemy();
            }

            foreach (var enemy in this.Enemies)
            {
                enemy.Update(gameTime);

                foreach (var arrow in this.PlayerRef.Arrows)
                {
                    if (arrow.CalculateArrowBounds().Intersects(enemy.CalculateEnemyBounds()) && enemy.IsAlive)
                    {
                        if (arrow.CalculateArrowBounds().Intersects(enemy.CalculateEnemyBalloonBounds()))
                        {
                            enemy.Die();
                            this.EnemiesKilled++;
                            arrow.Hit();
                        }
                        else
                        {
                            arrow.Deflect();
                        }
                    }
                }
            }
        }

        public void GenerateEnemy()
        {
            int positionX = Constants.Random.Next(1, Constants.ENEMY_MIN_POS_X);
            this.Enemies.Add(new Enemy(this.Content, Constants.ENEMY_WALKING_TEXTURE, Constants.Random.Next(Constants.ENEMY_MIN_SPEED, Constants.ENEMY_MAX_SPEED),
                new Vector2(-positionX, Constants.Random.Next(Constants.ENEMY_MIN_POS_Y, Constants.ENEMY_MAX_POS_Y)),
                Constants.ENEMY_FALLING_TEXTURE, Constants.ENEMY_DEAD_TEXTURE, Constants.ENEMY_ON_GROUND_WALKING_TEXTURE, Constants.ENEMY_ON_LADDER));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var enemy in this.Enemies)
            {
                enemy.Draw(spriteBatch);
            }
        }
    }
}
