﻿namespace PooyanGameTest.GameObjects.Entities
{
    using System.Collections.Generic;
    using FlyingObjects;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Utils;

    public class Player : Entity
    {
        public Player(ContentManager content, string textureName)
            : base(content, textureName)
        {
            this.Speed = Constants.PLAYER_SPEED;
            this.Position = new Vector2(Constants.PLAYER_POS_X, Constants.PLAYER_START_POS_Y);
            this.Rope = new Rope(content);
            this.Arrows = new List<Arrow>();
            this.GameOverDelay = new DelayedAction[4];
        }

        public List<Arrow> Arrows { get; set; }

        private Rope Rope { get; set; }

        private KeyboardState CurrentKeyboardState { get; set; }

        private KeyboardState PreviousKeyboardState { get; set; }

        private DelayedAction[] GameOverDelay { get; set; }

        public void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            this.HandleArrows(gameTime);

            if (this.IsAlive)
            {
                this.Rope.Update(this.Position);
                this.HandleInput(keyboardState);
                this.CheckGameOver();

                for (int i = 0; i < this.GameOverDelay.Length; i++)
                {
                    if (this.GameOverDelay[i] != null)
                    {
                        if (!this.GameOverDelay[i].Update((float)gameTime.ElapsedGameTime.TotalSeconds))
                        {
                            this.GameOverDelay[i] = null;
                        }
                    }
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (var arrow in this.Arrows)
            {
                arrow.Draw(spriteBatch);
            }

            this.Rope.Draw(spriteBatch);
            spriteBatch.Draw(this.Texture, this.Position, scale: new Vector2(1.7f, 1.7f));
        }

        public void Shoot()
        {
            this.Arrows.Add(new Arrow(this.Content, Constants.ARROW_SPEED, new Vector2(this.Position.X, this.Position.Y + (this.Texture.Height / 2))));
        }

        private void HandleInput(KeyboardState keyboardState)
        {
            this.PreviousKeyboardState = this.CurrentKeyboardState;
            this.CurrentKeyboardState = Keyboard.GetState();

            if (keyboardState.IsKeyDown(Keys.Up))
            {
                if (this.Position.Y >= Constants.PLAYER_MAX_TOP_POS_Y)
                {
                    this.Move(new Vector2(0, -this.Speed));
                }
            }
            else if (keyboardState.IsKeyDown(Keys.Down))
            {
                if (this.Position.Y <= Constants.PLAYER_MAX_BOT_POS_Y)
                {
                    this.Move(new Vector2(0, this.Speed));
                }
            }

            for (int i = 0; i < this.PreviousKeyboardState.GetPressedKeys().Length; i++)
            {
                if (this.PreviousKeyboardState.GetPressedKeys()[i] == Keys.Space && this.CurrentKeyboardState.IsKeyUp(Keys.Space))
                {
                    this.Shoot();
                }
            }
        }

        private void HandleArrows(GameTime gameTime)
        {
            for (int i = 0; i < this.Arrows.Count; i++)
            {
                this.Arrows[i].Update(gameTime);
                if (!this.Arrows[i].IsActive)
                {
                    this.Arrows.Remove(this.Arrows[i]);
                    i--;
                }
            }
        }

        private void CheckGameOver()
        {
            if (Constants.BusyLadders > 3)
            {
                this.IsAlive = false;
            }

            for (int i = 0; i < this.GameOverDelay.Length; i++)
            {
                if (Constants.BusyLadders == this.GameOverDelay.Length - i && this.GameOverDelay[i] == null && this.Position.Y > 135 + i * 65 && this.Position.Y < 170 + i * 65)
                {
                    int multiplier = i;
                    this.GameOverDelay[i] = new DelayedAction(() =>
                    {
                        if (this.Position.Y > 135 + multiplier * 65 && this.Position.Y < 170 + multiplier * 65)
                        {
                            this.IsAlive = false;
                        }
                    }, 1);
                }
            }
        }
    }
}
