﻿namespace PooyanGameTest.GameObjects.Entities
{
    using System.Collections.Generic;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;

    public abstract class Entity
    {
        public Entity(ContentManager content, string textureName)
        {
            this.Content = content;
            this.Texture = content.Load<Texture2D>(textureName);
            this.IsAlive = true;
        }

        public bool IsAlive { get; protected set; }

        public Vector2 Position { get; protected set; }

        protected Texture2D Texture { get; set; }

        protected int Speed { get; set; }

        protected ContentManager Content { get; set; }

        public virtual void Move(Vector2 relativeDirection)
        {
            this.Position += relativeDirection;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.Texture, this.Position, scale: new Vector2(1.9f, 1.9f));
        }
    }
}
