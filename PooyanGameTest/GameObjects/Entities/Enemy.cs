﻿namespace PooyanGameTest.GameObjects.Entities
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Utils;

    public enum EnemyStates
    {
        WALKING,
        FALLING,
        DEAD,
        ONGROUND,
        ONLADDER
    }

    public class Enemy : Entity
    {
        public Enemy(ContentManager content, string walkingTextureName, int speed, Vector2 position, string fallingTextureName, string deadTextureName,
            string onGroundWalkingTextureName, string climbingTextureName)
            : base(content, walkingTextureName)
        {
            this.AllTextures = new List<Texture2D>();
            this.AllTextures.Add(content.Load<Texture2D>(walkingTextureName));
            this.AllTextures.Add(content.Load<Texture2D>(fallingTextureName));
            this.AllTextures.Add(content.Load<Texture2D>(deadTextureName));
            this.AllTextures.Add(content.Load<Texture2D>(onGroundWalkingTextureName));
            this.AllTextures.Add(content.Load<Texture2D>(climbingTextureName));

            this.Animations = new Dictionary<string, Animation>();
            this.Animations.Add("Walking", new Animation(content.Load<Texture2D>(walkingTextureName), 2, new Vector2(20, 28), true));
            this.Animations.Add("Falling", new Animation(content.Load<Texture2D>(fallingTextureName), 1, new Vector2(18, 28), true));
            this.Animations.Add("Dead", new Animation(content.Load<Texture2D>(deadTextureName), 1, new Vector2(16, 16), true));
            this.Animations.Add("OnGroundWalking", new Animation(content.Load<Texture2D>(onGroundWalkingTextureName), 3, new Vector2(20, 16), true));
            this.Animations.Add("Climbing", new Animation(content.Load<Texture2D>(climbingTextureName), 8, new Vector2(15, 16), false));

            this.Speed = speed;
            this.Position = position;
            this.States = EnemyStates.WALKING;
            this.ActiveAnimation = this.Animations["Walking"];
            this.FallPositionX = Constants.Random.Next(Constants.ENEMY_FALL_POSITION_X_LEFT, Constants.ENEMY_FALL_POSITION_X_RIGHT);
        }

        private EnemyStates States { get; set; }

        private Dictionary<string, Animation> Animations { get; set; }

        private List<Texture2D> AllTextures { get; set; }

        private int FallPositionX { get; set; }

        private Animation ActiveAnimation { get; set; }

        public void Update(GameTime gameTime)
        {
            this.HandleMovement(gameTime);

            if (this.Position.X >= this.FallPositionX && this.States == EnemyStates.WALKING)
            {
                this.States = EnemyStates.FALLING;
                this.ActiveAnimation = this.Animations["Falling"];
            }
            else if (this.Position.X >= Constants.PLAYER_POS_X && this.States == EnemyStates.ONGROUND)
            {
                this.States = EnemyStates.ONLADDER;
                this.ActiveAnimation = this.Animations["Climbing"];
            }

            if (this.Position.Y >= Constants.GROUND_POSITION_Y && this.States == EnemyStates.FALLING)
            {
                this.States = EnemyStates.ONGROUND;
                this.ActiveAnimation = this.Animations["OnGroundWalking"];
            }

            if (this.ActiveAnimation != this.Animations["Climbing"])
            {
                this.ActiveAnimation.Update(gameTime, this.Position);
            }
            else if (!this.ActiveAnimation.Loop)
            {
                this.ActiveAnimation.Update(gameTime, this.Position);
            }
        }

        public Rectangle CalculateEnemyBounds()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, (int)((this.ActiveAnimation.FrameBounds.Width * 1.7) - 1), (int)(this.ActiveAnimation.FrameBounds.Height * 1.9));
        }

        public Rectangle CalculateEnemyBalloonBounds()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, (int)(this.ActiveAnimation.FrameBounds.Width * 1.7), this.ActiveAnimation.FrameBounds.Height);
        }

        public void Die()
        {
            this.IsAlive = false;
            this.States = EnemyStates.DEAD;
            this.ActiveAnimation = this.Animations["Dead"];
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            this.ActiveAnimation.Draw(spriteBatch);
        }

        private void HandleMovement(GameTime gameTime)
        {
            if (this.States == EnemyStates.WALKING)
            {
                this.Move(new Vector2(this.Speed, 0));
            }
            else if (this.States == EnemyStates.FALLING)
            {
                this.Move(new Vector2(0, (float)(this.Speed * 0.5)));
            }
            else if (this.States == EnemyStates.DEAD)
            {
                this.Move(new Vector2(0, this.Speed));
            }
            else if (this.States == EnemyStates.ONGROUND)
            {
                this.Move(new Vector2(this.Speed, 0));
            }
            else if (this.States == EnemyStates.ONLADDER && this.Position.X != Constants.LADDER_POSITION_X)
            {
                if (Constants.BusyLadders == 0)
                {
                    this.Position = new Vector2(Constants.LADDER_POSITION_X, Constants.LadderPositionsY[0]);
                }
                else if (Constants.BusyLadders == 1)
                {
                    this.Position = new Vector2(Constants.LADDER_POSITION_X, Constants.LadderPositionsY[1]);
                }
                else if (Constants.BusyLadders == 2)
                {
                    this.Position = new Vector2(Constants.LADDER_POSITION_X, Constants.LadderPositionsY[2]);
                }
                else
                {
                    this.Position = new Vector2(Constants.LADDER_POSITION_X, Constants.LadderPositionsY[3]);

                    // Game Over
                }
                Constants.BusyLadders++;
            }
        }
    }
}
