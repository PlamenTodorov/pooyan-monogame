﻿namespace PooyanGameTest.GameObjects
{
    using System.Collections.Generic;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Utils;

    public class Rope
    {
        public Rope(ContentManager content)
        {
            this.Content = content;
            this.Parts = new List<Texture2D>();
            this.Parts.Add(this.Content.Load<Texture2D>(Constants.ROPE_TEXTURE_NAME));
            this.PartHeight = this.Parts[0].Width;
            this.PartWidth = this.Parts[0].Height;
        }

        public List<Texture2D> Parts { get; set; }

        private ContentManager Content { get; set; }

        private int PartWidth { get; set; }

        private int PartHeight { get; set; }

        public void Update(Vector2 playerPosition)
        {
            int partsCount = (int)(playerPosition.Y - Constants.ROPE_POSITION_START_Y) / this.PartHeight;

            while (partsCount != this.Parts.Count)
            {
                if (partsCount > this.Parts.Count)
                {
                    this.Parts.Add(this.Content.Load<Texture2D>(Constants.ROPE_TEXTURE_NAME));
                }
                else
                {
                    this.Parts.Remove(this.Parts[this.Parts.Count - 1]);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < this.Parts.Count; i++)
            {
                spriteBatch.Draw(this.Parts[i], new Vector2(Constants.ROPE_POSITION_START_X, Constants.ROPE_POSITION_START_Y + (i * this.PartHeight)));
            }
        }
    }
}
