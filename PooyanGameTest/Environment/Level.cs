﻿namespace PooyanGameTest.Environment
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;

    public class Level
    {
        public Level(ContentManager content, string backgroundName, int gameWidth, int gameHeight)
        {
            this.Background = content.Load<Texture2D>(backgroundName);
            this.BgRect = new Rectangle(0, 0, gameWidth, gameHeight);
        }

        private Rectangle BgRect { get; set; }

        private Texture2D Background { get; set; }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.Background, this.BgRect, Color.White);
        }
    }
}
